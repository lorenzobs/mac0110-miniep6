# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    # escreva sua solução aqui
NumeroDeImpares = 1
ImparInicial = 1
ProxImpar = ImparInicial
soma = ImparInicial
cubo = n * n * n

    while (soma != cubo)  
        
        if (soma < cubo)
            NumeroDeImpares = NumeroDeImpares + 1
            ProxImpar = ProxImpar + 2
            soma = soma + ProxImpar         
        end
        
        if (soma > cubo || NumeroDeImpares > n )
            ImparInicial = ImparInicial + 2
            soma = ImparInicial
            NumeroDeImpares = 1
            ProxImpar = ImparInicial
        end

   end
return ImparInicial
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    # escreva sua solução aqui
NumeroDeImpares = 1
ImparInicial = 1
ProxImpar = ImparInicial
soma = ImparInicial
cubo = m * m * m

    while (soma != cubo)  
        
        if (soma < cubo)
            NumeroDeImpares = NumeroDeImpares + 1
            ProxImpar = ProxImpar + 2
            soma = soma + ProxImpar         
        end
        
        if (soma > cubo || NumeroDeImpares > m )
            ImparInicial = ImparInicial + 2
            soma = ImparInicial
            NumeroDeImpares = 1
            ProxImpar = ImparInicial
        end

    end

print(m, " ", cubo, " ")
NumeroImpar = ImparInicial
i = 1
   while (i <= m)
        print(NumeroImpar, " ")
        NumeroImpar = NumeroImpar + 2
        i = i + 1
    end
println()
end

function mostra_n(n)
    # escreva sua solução aqui
m = 1
    while ( m <= n)
        imprime_impares_consecutivos(m)
        m = m + 1
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()